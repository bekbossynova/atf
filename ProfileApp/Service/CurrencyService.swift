
//  CurrencyService.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/19/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit
import Moya

enum CurrencyService {
    case exchangeRate(currency: String)
}


extension CurrencyService: TargetType {
    
    var baseURL: URL {
        switch self {
        case .exchangeRate:
            guard let url = URL(string: SettingsConstant.BASE_URL) else { fatalError(NSLocalizedString("Base auth url is not configured", comment: "Base url")) }
            print(url)
            return url
        }
    }
    
    var path: String {
        switch self {
        case .exchangeRate:
            return "/latest"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .exchangeRate:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .exchangeRate(let currency):
            return .requestParameters(parameters: ["base": currency.localizedUppercase], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String: String]? {
        return [:]
    }
    
}
