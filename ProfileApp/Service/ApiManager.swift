//
//  ApiManager.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/20/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import Foundation
import Moya

class ApiManager {
    
    static let shared =  ApiManager()
    
    let currencyService = MoyaProvider<CurrencyService>()
    
    func updateExchangeRate(currency: String, completion:@escaping (CurrencyResponse?, ErrorResponse?) -> ()) {
        let exchangeRate = CurrencyService.exchangeRate(currency: currency)
        return fetchData(service: exchangeRate, completion: completion)
        
    }
    
    //MARK: -Fetch
    func fetchData<T: Decodable> (service: CurrencyService, completion:@escaping (T?, ErrorResponse?) -> ()) {
        currencyService.request(service) { result in
            switch result {
            case let .success(moyaResponse):
                if(moyaResponse.statusCode == 200){
                    switch T.self {
                    case is Int.Type:
                        completion(moyaResponse.statusCode as? T, nil)
                    case is CurrencyResponse.Type:
                        do {
                            let currencyJson = try JSONDecoder().decode(CurrencyResponse.self, from: moyaResponse.data)
                            let rate = CurrencyResponse(rates: currencyJson.rates, base: currencyJson.base, date: currencyJson.date)
                            print(rate)
                            completion((rate as? T), nil)
                        } catch {
                            print(error.localizedDescription)
                        }
                    default:
                        completion(self.decodedObject(object: moyaResponse.data), nil)
                    }
                }
                else{
                    switch T.self {
                    case is Int.Type:
                        completion(moyaResponse.statusCode as? T, self.decodedErrorObject(object: moyaResponse.data))
                    default:
                        completion(nil, self.decodedErrorObject(object: moyaResponse.data))
                    }
                }
                
                break
            case let .failure(error):
                completion(nil, self.convertToError(object: error))
                break
            }
        }
        
    }
    
    
    //MARK: - ERROR
    
    func convertToError (object: MoyaError) -> ErrorResponse?{
        if(object.errorDescription == "URLSessionTask failed with error: The request timed out."){
            let errorResponse = ErrorResponse(name: NSLocalizedString("Failed.", comment: ""), message:  NSLocalizedString("The Internet connection appears to be offline.", comment: ""), code: 0, status: object.errorCode)
            return errorResponse
        }
        else{
            let errorResponse = ErrorResponse(name: NSLocalizedString("Failed.", comment: ""), message: object.errorDescription ?? NSLocalizedString("Unrecognized error", comment: ""), code: 0, status: object.errorCode)
            return errorResponse
        }
        
    }
    func decodedErrorObject (object: Data) -> ErrorResponse?{
        guard let json = try? JSONSerialization.jsonObject(with: object, options: .mutableContainers) as? NSDictionary
            else { return nil }
        let errorResponse = ErrorResponse(json: json as! [String : Any])
        return errorResponse
    }
    
    func decodedObject<T: Decodable> (object: Data) -> T?{
        do{
            let jsonDecoder = JSONDecoder()
            let errorResponse = try jsonDecoder.decode(T.self, from: object)
            return errorResponse
        }
        catch{
            return nil
        }
    }
    
    
    
    
    
    
    
}
