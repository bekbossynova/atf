//
//  Service.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/20/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit
let dispatchGroup = DispatchGroup()

class Service {
    
    static let shared: Service = Service()
    
    var dataSource: CurrencyResponse?
    
    
    func fethUpdateExchangeRate(parent: UIViewController)  {
        ApiManager.shared.updateExchangeRate(currency: "usd") { (response, errorResponse) in
            DispatchQueue.main.async {
                print("thread started")
                if (response != nil && errorResponse == nil ){
                    guard let responseJson = response else { return }
                    self.dataSource = responseJson
                    print("DATA SOURCE", self.dataSource as Any)
                } else {
                    ExceptionHelper.showErrorMessage(messageTitle: errorResponse?.name ?? NSLocalizedString("Failed.", comment: ""), errorMessage: errorResponse?.message ?? NSLocalizedString("The Internet connection appears to be offline.", comment: ""), buttonText: "Ok", parent: parent)
                }
                dispatchGroup.leave()
            }
        }
    }
}
