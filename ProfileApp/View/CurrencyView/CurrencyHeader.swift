//
//  CurrencyHeader.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/20/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class CurrencyHeader: UIView {
    
    let headerTitle: UILabel = {
        let label            = UILabel()
        label.text           = "Курс валют"
        label.font           = UIFont.systemFont(ofSize: 34, weight: .bold)
        return label
    }()
    
    
    let currentDateLabel    = UILabel()
    let saleLabel           = UILabel()
    let purchaseLabel       = UILabel()
    let viewLine            = UIView()
    
    let defaultPadding: CGFloat = 16
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        labelsConfigure()
        layoutConfigure()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func labelsConfigure() {
        let labels = [currentDateLabel, saleLabel]
        saleLabel.text        = "Покупка | Продажа"
        labels.forEach { (label) in
            label.textColor = UIColor(red: 0.616, green: 0.675, blue: 0.725, alpha: 1)
            label.font      = UIFont.systemFont(ofSize: 14, weight: .regular)
        }
    }
    
    fileprivate func layoutConfigure(){
        let stackView = UIStackView(arrangedSubviews: [
            currentDateLabel, UIView(), saleLabel])
        stackView.axis         = .horizontal
        stackView.distribution = .equalSpacing
        
        addSubview(headerTitle)
        headerTitle.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: defaultPadding, left: defaultPadding, bottom: defaultPadding, right: defaultPadding) )
        
        addSubview(stackView)
        stackView.anchor(top: headerTitle.bottomAnchor, leading: headerTitle.leadingAnchor, bottom: nil, trailing: headerTitle.trailingAnchor)
        
        addSubview(viewLine)
        viewLine.constrainHeight(constant: 1)
        viewLine.layer.borderWidth = 1
        viewLine.layer.borderColor = UIColor(red: 0.965, green: 0.965, blue: 0.965, alpha: 1).cgColor
        viewLine.anchor(top: stackView.bottomAnchor, leading: stackView.leadingAnchor, bottom: bottomAnchor, trailing: stackView.trailingAnchor, padding: .init(top: defaultPadding, left: 0, bottom: 0, right: 0))
        
    }
}

