//
//  CurrencyFooter.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/20/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class CurrencyFooter: UIView {
    
    let line = UIView()
    
    let currencyIcon : UIImageView = {
        let iv = UIImageView()
        iv.contentMode   = .scaleAspectFill
        iv.clipsToBounds = true
        iv.constrainHeight(constant: 32)
        iv.constrainWidth(constant: 48)
        iv.image = UIImage(named: "Main_react")?.withRenderingMode(.alwaysOriginal)
        return iv
    }()
    
    let currencyConversionButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Конвертация валют", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setImage(UIImage(named: "Main_react")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        button.setInsets(forContentPadding: .init(top: 16, left: 16, bottom: 16, right: 0), imageTitlePadding: 16)
        return button
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutConfigure()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func layoutConfigure() {
        addSubview(line)
        line.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor)
        line.constrainHeight(constant: 1)
        line.layer.borderWidth = 1
        line.layer.borderColor = UIColor(red: 0.965, green: 0.965, blue: 0.965, alpha: 1).cgColor
        
        addSubview(currencyConversionButton)
        currencyConversionButton.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        
    }
    
}

