//
//  CurrencyCell.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/20/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit


class CurrencyCell: UITableViewCell  {
    
    var updateData: UpdateCell? {
        didSet {
            guard let update = updateData else { return }
            textLabel?.text        = update.description
            guard let updateExchange = update.updateCellExchangeRate else { return }
            let changedExchange = Double(round(1000 * updateExchange) / 1000)
            exchangeRateLabel.text = "\(changedExchange) | \(changedExchange)"
        }
    }
    
    
    static let reuseIdentifier = "reuseIdentifier"
    
    let exchangeRateLabel: UILabel = {
        let label = UILabel()
        label.text = "\(0) | \(0)"
        label.font = UIFont.systemFont(ofSize: 17, weight: .regular)
        return label
    }()
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(exchangeRateLabel)
        exchangeRateLabel.anchor(top: topAnchor, leading: nil, bottom: nil, trailing: trailingAnchor, padding: .init(top: 26, left: 0, bottom: 0, right: 16))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}



