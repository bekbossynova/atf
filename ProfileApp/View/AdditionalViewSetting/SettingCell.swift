//
//  SettingCell.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/19/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {
    
    //MARK: -Properties
    static let reuseIdentifier = "reuseIdentifier"
    
    
    var sectionType: SectionType? {
        didSet {
            guard let sectionType = sectionType else { return }
            textLabel?.text = sectionType.description
        }
    }
    
    //MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
