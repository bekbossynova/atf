//
//  ProfileInfoHeader.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/19/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class AdditionalInfoHeader: UIView {
    
    //MARK: -Properties
    let headerTitle: UILabel = {
        let label            = UILabel()
        label.text           = "Ещё"
        label.textColor      = .white
        label.font           = UIFont.systemFont(ofSize: 34, weight: .bold)
        return label
    }()
    
    let profileImageView: UIImageView = {
        let iv                        = UIImageView()
        iv.contentMode                = .scaleAspectFill
        iv.clipsToBounds              = true
        iv.backgroundColor            = .lightGray
        iv.layer.cornerRadius         = 64 / 2
        iv.constrainWidth(constant: 64)
        iv.constrainHeight(constant: 64)
        return iv
    }()
    
    let photoButton: UIButton = {
        let button = UIButton(type: .system)
        button.contentMode                = .scaleAspectFill
        button.clipsToBounds              = true
        button.setImage(UIImage(named: "photo_icon")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.constrainWidth(constant: 16)
        button.constrainHeight(constant: 16)
        return button
    }()
    
    let userNameLabel = UILabel ()
    let userNumberLabel = UILabel ()
    let defaultPadding: CGFloat = 16
    
    //MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setGradientBackground()
        profileLabelsConfigure()
        layoutConfigure()
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Functions
    func setGradientBackground() {
        let startColor =  UIColor(red: 0.235, green: 0.612, blue: 0.478, alpha: 1).cgColor
        let endColor   =  UIColor(red: 0.145, green: 0.227, blue: 0.369, alpha: 0.8).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [startColor, endColor]
        gradientLayer.locations = [0, 1]
        gradientLayer.startPoint = CGPoint(x: 0.25, y: 0.2)
        gradientLayer.endPoint = CGPoint(x: 0.75, y: 0.5)
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at:0)
    }
    
    func profileLabelsConfigure() {
        let profileLabels = [userNameLabel, userNumberLabel]
        userNameLabel.text = "Привалов Дмитрий Юрьевич"
        userNumberLabel.text  = "+7 777 207 00 57"
        profileLabels.forEach { (label) in
            label.textColor       = .white
            label.numberOfLines   = 0
            label.lineBreakMode   = .byWordWrapping
            label.font            = UIFont.systemFont(ofSize: 17, weight: .medium)
        }
    }
    
    func layoutConfigure() {
        let stackView = UIStackView(arrangedSubviews: [userNameLabel, userNumberLabel])
        stackView.distribution  = .fill
        stackView.alignment     = .fill
        stackView.axis          = .vertical
        
        addSubview(headerTitle)
        headerTitle.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor, padding: .init(top: 46, left: defaultPadding, bottom: 0, right: 0))
        
        addSubview(profileImageView)
        profileImageView.anchor(top: headerTitle.bottomAnchor, leading: headerTitle.leadingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: defaultPadding, left: 0, bottom: defaultPadding, right: 0))
        
        addSubview(photoButton)
        photoButton.anchor(top: nil, leading: nil, bottom: profileImageView.bottomAnchor, trailing: profileImageView.trailingAnchor, padding: .init(top: 0, left: -5, bottom: 0, right: 0))
        
        addSubview(stackView)
        stackView.anchor(top: nil, leading: profileImageView.trailingAnchor, bottom: bottomAnchor, trailing: nil, padding: .init(top: 0, left: defaultPadding, bottom: 26, right: defaultPadding))
        
        
    }
    
}
