//
//  AdditionInfoFooter.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/20/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class AdditionInfoFooter: UIView {
    
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    
    
    let currentVersionLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(red: 0.584, green: 0.665, blue: 0.733, alpha: 1)
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        guard let appVer = appVersion else { return }
        currentVersionLabel.text = "Версия: \(appVer)"
        
        addSubview(currentVersionLabel)
        currentVersionLabel.centerInSuperview()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
