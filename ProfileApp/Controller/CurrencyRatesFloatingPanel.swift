//
//  CurrencyRatesFloatingPanel.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/20/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

class CurrencyRatesFloatingPanel: UIViewController {
    
    var tableView: UITableView!
    var currencyHeader: CurrencyHeader!
    var currencyFooter: CurrencyFooter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }
    
    func configureTableView() {
        tableView               = UITableView()
        tableView.delegate      = self
        tableView.dataSource    = self
        tableView.rowHeight     = 64
        tableView.isScrollEnabled = false
        
        
        tableView.register(CurrencyCell.self, forCellReuseIdentifier: CurrencyCell.reuseIdentifier)
        view.addSubview(tableView)
        tableView.frame = view.frame
        let headerFrame = CGRect(x: 0, y: 80, width: view.frame.width, height: 140)
        let footerFrame = CGRect(x: 0, y: 80, width: view.frame.width, height: 140)
        currencyHeader = CurrencyHeader(frame: headerFrame)
        currencyFooter = CurrencyFooter(frame: footerFrame)
        tableView.tableHeaderView = currencyHeader
        tableView.tableFooterView = currencyFooter
        
    }
    override func viewWillAppear(_ animated: Bool) {
        var currencyChangeDate = Service.shared.dataSource?.date
        let date = Date()
        let dateString = date.getFormattedDateString(format: "yyyy-MM-dd")
        if(currencyChangeDate == nil){
            currencyChangeDate = dateString
        }
        currencyHeader.currentDateLabel.text = "На \(currencyChangeDate!)"
    }
}

extension CurrencyRatesFloatingPanel: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CurrencyInfoOption.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyCell.reuseIdentifier, for: indexPath) as! CurrencyCell
        guard let section = CurrencyInfoOption(rawValue: indexPath.row) else { return UITableViewCell() }
        switch section {
        case .EUR:
            let eur = CurrencyInfoOption(rawValue: indexPath.row)
            cell.updateData = eur
        case .USD:
            let usd = CurrencyInfoOption(rawValue: indexPath.row)
            cell.updateData = usd
        case .RUB:
            let rub = CurrencyInfoOption(rawValue: indexPath.row)
            cell.updateData = rub
            
            
        }
        
        return cell
    }
    
    
}
