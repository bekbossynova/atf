//
//  CurrencyFloatingPanelLayout.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/20/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit
import FloatingPanel


// MARK:- Floating Panel
class CurrencyFloatingPanelLayout: FloatingPanelLayout {
    
    public var initialPosition: FloatingPanelPosition {
        return .half
    }
    
    public var supportedPositions: Set<FloatingPanelPosition> {
        return [.half, .hidden]
    }
    
    public func insetFor(position: FloatingPanelPosition) -> CGFloat? {
        switch position {
        case .half: return 395
        default: return nil
        }
    }
    
    public func backdropAlphaFor(position: FloatingPanelPosition) -> CGFloat {
        return 0.2
    }
}


extension AdditionalInfoViewController: FloatingPanelControllerDelegate {
    
    public func openFpc() {
        print("opening FPC")
        view.addSubview(fpc.view)
        fpc.view.fillSuperview()
        fpc.view.frame = self.view.bounds
        self.addChild(fpc)
        fpc.show(animated: true) {
            self.fpc.didMove(toParent: self)
            self.floatTrue = true
        }
    }
    
    func tapGestureRecognizerAction() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOpenCurrencyVC))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        self.view.isUserInteractionEnabled = true
    }
    
    func currencyFloationController() {
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = .clear
        fpc.surfaceView.grabberHandle.barColor = .clear
        
        if #available(iOS 11, *) {
            fpc.surfaceView.cornerRadius = 20.0
        } else {
            fpc.surfaceView.cornerRadius = 20.0
        }
        fpc.surfaceView.shadowHidden = false
        fpc.set(contentViewController: contentVC)
        fpc.track(scrollView: contentVC.tableView)
        fpc.contentMode = .fitToBounds
        
    }
    
    @objc func handleOpenCurrencyVC(_ sender: UITapGestureRecognizer) {
        if self.floatTrue {
            fpc.willMove(toParent: nil)
            fpc.hide(animated: true) {
                self.fpc.view.removeFromSuperview()
                self.fpc.removeFromParent()
                self.floatTrue = false
            }
        }
    }
    
    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        switch vc {
        case fpc:
            switch newCollection.verticalSizeClass {
            case .compact:
                fpc.surfaceView.borderWidth = 1.0 / traitCollection.displayScale
                fpc.surfaceView.borderColor = UIColor.black.withAlphaComponent(0.2)
                return CurrencyFloatingPanelLayout()
            default:
                fpc.surfaceView.borderWidth = 0.0
                fpc.surfaceView.borderColor = nil
                return CurrencyFloatingPanelLayout()
            }
        default:
            break
            
        }
        return CurrencyFloatingPanelLayout()
    }
    
}
