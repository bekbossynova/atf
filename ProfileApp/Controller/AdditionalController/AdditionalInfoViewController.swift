//
//  ViewController.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/19/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit
import FloatingPanel

class AdditionalInfoViewController: UIViewController {
    
    var tableView: UITableView!
    var additionalInfoHeader: AdditionalInfoHeader!
    var additionInfoFooter: AdditionInfoFooter!
    let acactivityIndi = UIActivityIndicatorView()
    
    
    var fpc: FloatingPanelController!
    var contentVC = CurrencyRatesFloatingPanel()
    var floatTrue = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        currencyFloationController()
        tapGestureRecognizerAction()
        
    }
    
    func configureTableView() {
        tableView               = UITableView()
        tableView.delegate      = self
        tableView.dataSource    = self
        tableView.rowHeight     = 64
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.allowsMultipleSelection = false
        
        tableView.register(SettingCell.self, forCellReuseIdentifier: SettingCell.reuseIdentifier)
        view.addSubview(tableView)
        tableView.frame = view.frame
        let frame = CGRect(x: 0, y: 80, width: view.frame.width, height: 200)
        additionalInfoHeader = AdditionalInfoHeader(frame: frame)
        additionInfoFooter   = AdditionInfoFooter(frame: frame)
        tableView.tableHeaderView = additionalInfoHeader
        tableView.tableFooterView = additionInfoFooter
        tableView.backgroundColor = UIColor(red: 0.898, green: 0.898, blue: 0.898, alpha: 1)
        
    }
    
    
}



