//
//  AdditionalTableView.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/20/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

extension AdditionalInfoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let section = AdditionInfoSection(rawValue: indexPath.section) else { return }
        let cell : SettingCell = (tableView.cellForRow(at: indexPath) as! SettingCell)
        cell.addSubview(acactivityIndi)
        acactivityIndi.anchor(top: nil, leading: nil, bottom: cell.bottomAnchor, trailing: cell.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 25, right: 16))
        cell.accessoryView = acactivityIndi
        if(cell.selectionStyle == .none) {
            return
        }
        switch section {
        case .Section_Info:
            print(section.description)
            if indexPath.row == 0 {
                acactivityIndi.startAnimating()
                dispatchGroup.enter()
                Service.shared.fethUpdateExchangeRate(parent: self)
                dispatchGroup.notify(queue: .main) {
                    print("thread finished")
                    self.openFpc()
                    self.acactivityIndi.stopAnimating()
                }
            }
        case .Section_Setting:
            print("\(section.description)")
        case .Section_LogOut:
            print("\(section.description)")
        }
        cell.isSelected = false
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let section = AdditionInfoSection(rawValue: indexPath.section)
        let cell : SettingCell = (tableView.cellForRow(at: indexPath) as! SettingCell)
        switch section {
        case .Section_Info:
            switch indexPath.row {
            case 0:
                cell.isUserInteractionEnabled = true
            default:
                cell.isUserInteractionEnabled = false
                cell.selectionStyle = .none
            }
            return indexPath
        default:
            cell.isUserInteractionEnabled = false
            cell.selectionStyle = .none
            return nil
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return AdditionInfoSection.allCases.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = AdditionInfoSection(rawValue: section) else { return 0 }
        switch section {
        case .Section_Info: return InfoOptions.allCases.count
        case .Section_Setting: return SettingOptions.allCases.count
        case .Section_LogOut: return LogOutOptions.allCases.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SettingCell.reuseIdentifier, for: indexPath) as! SettingCell
        
        guard let section = AdditionInfoSection(rawValue: indexPath.section) else { return UITableViewCell() }
        if(indexPath.section != 1 && indexPath.row != 0){
            cell.isUserInteractionEnabled = false
        }
        else{
            cell.selectionStyle = .default
        }
        switch section {
        case .Section_Info:
            let info = InfoOptions(rawValue: indexPath.row)
            cell.sectionType = info
        case .Section_Setting:
            let setting = SettingOptions(rawValue: indexPath.row)
            cell.sectionType =  setting
        case .Section_LogOut:
            let logout  = LogOutOptions(rawValue: indexPath.row)
            cell.sectionType = logout
            cell.textLabel?.textColor   = .red
        }
        return cell
    }
    
}

//MARK: - TableView Section

extension AdditionalInfoViewController {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView()
        let sectionTitle        = UILabel()
        sectionTitle.textColor  = .systemGray
        sectionTitle.font       = UIFont.systemFont(ofSize: 12, weight: .regular)
        sectionTitle.text       = AdditionInfoSection(rawValue: section)?.description
        
        view.addSubview(sectionTitle)
        sectionTitle.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 16, bottom: 5, right: 0))
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let section = AdditionInfoSection(rawValue: section) else { return 0 }
        switch section {
        case .Section_Info: return SectionHelper.SECTION_INFO_HEIGHT
        case .Section_Setting: return SectionHelper.SECTION_SETTINGS_HEIGHT
        case .Section_LogOut: return SectionHelper.SECTION_LOGOUT_HEIGHT
        }
    }
    
}



