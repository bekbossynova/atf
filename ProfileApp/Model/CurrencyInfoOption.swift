//
//  CurrencyInfoSection.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/20/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import Foundation

protocol UpdateCell: CustomStringConvertible {
    var updateCellExchangeRate: Double? { get }
}

enum CurrencyInfoOption: Int, CaseIterable, CustomStringConvertible, UpdateCell {
    case USD
    case EUR
    case RUB
    
    var description: String {
        switch self {
        case .USD: return "Доллар (USD)"
        case .EUR: return "Евро (EUR)"
        case .RUB: return "Рубли (RUB)"
        }
    }
    
    var updateCellExchangeRate: Double? {
        guard let currency = Service.shared.dataSource else {return 0.0 }
        print("2DATA SOURCE", currency)
        switch self {
        case .USD: return currency.rates.USD
        case .EUR: return currency.rates.EUR
        case .RUB: return currency.rates.RUB
        }
        
    }
    
}
