//
//  ProfileSection.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/19/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import Foundation

protocol SectionType: CustomStringConvertible {}


enum AdditionInfoSection: Int, CaseIterable, CustomStringConvertible {
    case Section_Info
    case Section_Setting
    case Section_LogOut
    
    var description: String {
        switch self {
        case .Section_Info: return "АТФБАНК"
        case .Section_Setting: return ""
        case .Section_LogOut: return ""
        }
    }
}


enum InfoOptions: Int, CaseIterable, CustomStringConvertible, SectionType {
    
    case Currency
    case ContactInformation
    case ATMsAndBranches
    
    var description: String {
        switch self {
        case .Currency: return "Курс валют"
        case .ContactInformation: return "Контактная информация"
        case .ATMsAndBranches: return "Банкоматы и отделения"
        }
    }
    
}

enum SettingOptions: Int, CaseIterable, CustomStringConvertible, SectionType {
    case Setting
    
    
    var description: String {
        switch self {
        case .Setting: return "Настройки"
        }
    }
    
}

enum LogOutOptions: Int, CaseIterable, CustomStringConvertible, SectionType {
    case LogOut
    
    var description: String {
        switch self {
        case .LogOut: return "Выйти из аккаунта"
        }
    }
}
