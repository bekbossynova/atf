//
//  CurrencyResponse.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/19/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import Foundation

struct CurrencyResponse: Decodable {
    let rates: Rates
    let base, date: String
}

struct Rates: Decodable {
    let USD: Double
    let EUR: Double
    let RUB: Double
    
}
