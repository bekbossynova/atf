//
//  SectionHelper.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/19/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

struct SectionHelper {
    static let SECTION_INFO_HEIGHT:CGFloat = 40
    static let SECTION_SETTINGS_HEIGHT:CGFloat = 16
    static let SECTION_LOGOUT_HEIGHT:CGFloat = 16
}



