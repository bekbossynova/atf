//
//  ButtonSpacing.swift
//  ProfileApp
//
//  Created by bekbossynova on 8/20/20.
//  Copyright © 2020 bekbossynova. All rights reserved.
//

import UIKit

extension UIButton {
    
    func setInsets(
        forContentPadding contentPadding: UIEdgeInsets,
        imageTitlePadding: CGFloat) {
        self.contentEdgeInsets = UIEdgeInsets(
            top: contentPadding.top,
            left: contentPadding.left,
            bottom: contentPadding.bottom,
            right: contentPadding.right
        )
        self.titleEdgeInsets = UIEdgeInsets(
            top: 0,
            left: imageTitlePadding,
            bottom: 0,
            right: -imageTitlePadding
        )
    }
}
